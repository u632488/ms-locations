import { Injectable, NotAcceptableException } from '@nestjs/common';

@Injectable()
export class LocationsService {

  private locationsMock = [
    {
      _id: '60f6c70d08198e00097c3518',
      id: 5003,
      country: 'ARGENTINA',
      province: 'CAPITAL FEDERAL',
      region: {
        id: 'R01',
        name: 'Region 1',
      },
      department: 'CAPITAL FEDERAL',
      locality: 'CAPITAL FEDERAL',
      toptraffic: true,
    },
    {
      _id: '60f6c3b9cf4af200081c1367',
      id: 1592,
      country: 'ARGENTINA',
      province: 'CAPITAL FEDERAL',
      region: {
        id: 'R02',
        name: 'Region 2',
      },
      department: 'CAPITAL FEDERAL',
      locality: 'CAPITAL FEDERAL',
      toptraffic: true,
    },
    {
      _id: '63151e036a36a40009e7ca99',
      id: 8850,
      country: 'ARGENTINA',
      province: 'SALTA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'GRAL JOSE DE SAN MARTIN',
      locality: 'CAPIAZUTI',
      toptraffic: false,
    },
    {
      _id: '60f6c427db92a1000727505b',
      id: 2032,
      country: 'ARGENTINA',
      province: 'CORDOBA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'TOTORAL',
      locality: 'CAPILLA DE SITON',
      toptraffic: false,
    },
    {
      _id: '60f6c3c1cf4af200081c1374',
      id: 1623,
      country: 'ARGENTINA',
      province: 'CATAMARCA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'ANDALGALA',
      locality: 'CAPILLITAS',
      toptraffic: false,
    },
    {
      _id: '60f6c5c29182f60009f4cbfe',
      id: 3698,
      country: 'ARGENTINA',
      province: 'MISIONES',
      region: {
        id: 'R01',
        name: 'Region 1',
      },
      department: 'LIBERTADOR GRAL SAN MARTIN',
      locality: 'CAPIOVY',
      toptraffic: false,
    },
    {
      _id: '63147209e14d260009b05c8b',
      id: 8507,
      country: 'ARGENTINA',
      province: 'MISIONES',
      region: {
        id: 'R01',
        name: 'Region 1',
      },
      department: 'LIBERTADOR GRAL SAN MARTIN',
      locality: 'CAPIOVY',
      toptraffic: false,
    },
    {
      _id: '6313126848e52d00090f116e',
      id: 7287,
      country: 'ARGENTINA',
      province: 'CORRIENTES',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'MERCEDES',
      locality: 'CAPITA MINI',
      toptraffic: false,
    },
    {
      _id: '631212ff050bf3000902d0eb',
      id: 10451,
      country: 'ARGENTINA',
      province: 'TUCUMAN',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'MONTEROS',
      locality: 'CAPITAN CACERES',
      toptraffic: false,
    },
    {
      _id: '6314f57c28e68400095650a4',
      id: 5737,
      country: 'ARGENTINA',
      province: 'BUENOS AIRES',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'PEHUAJO',
      locality: 'CAPITAN CASTRO',
      toptraffic: false,
    },
    {
      _id: '60f6c631d8e028000a083b7d',
      id: 4133,
      country: 'ARGENTINA',
      province: 'SALTA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'RIVADAVIA',
      locality: 'CAPITAN JUAN PAGE',
      toptraffic: false,
    },
    {
      _id: '631179587cfb090009406dd8',
      id: 9149,
      country: 'ARGENTINA',
      province: 'SAN JUAN',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'RAWSON',
      locality: 'CAPITAN LAZO',
      toptraffic: false,
    },
    {
      _id: '60f6c40b3f8ab400095bccb3',
      id: 1916,
      country: 'ARGENTINA',
      province: 'CHACO',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'SARGENTO CABRAL',
      locality: 'CAPITAN SOLARI',
      toptraffic: false,
    },
    {
      _id: '6311df5fbb6ab4000983a09d',
      id: 8030,
      country: 'ARGENTINA',
      province: 'LA RIOJA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'CAPITAL',
      locality: 'CARRIZAL CAPITAL',
      toptraffic: false,
    },
    {
      _id: '60f6c4369011fa0008269d92',
      id: 2098,
      country: 'ARGENTINA',
      province: 'CORDOBA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'SANTA MARIA',
      locality: 'COSME CAPILLA DE COSME',
      toptraffic: false,
    },
    {
      _id: '60f6c76616496b00076c5ede',
      id: 5369,
      country: 'ARGENTINA',
      province: 'BUENOS AIRES',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'FLORENCIO VARELA',
      locality: 'LA CAPILLA',
      toptraffic: false,
    },
    {
      _id: '6313c3e7fc86680009195847',
      id: 9041,
      country: 'ARGENTINA',
      province: 'SALTA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'ROSARIO DE LERMA',
      locality: 'LAS CAPILLAS',
      toptraffic: false,
    },
    {
      _id: '60f6c642d8e028000a083b8c',
      id: 4194,
      country: 'ARGENTINA',
      province: 'SALTA',
      region: {
        id: 'R06',
        name: 'Region 6',
      },
      department: 'ROSARIO DE LERMA',
      locality: 'LAS CAPILLAS',
      toptraffic: false,
    },
  ];

  // create(createLocationDto: CreateLocationDto) {
    //   return 'This action adds a new location';
    // }
    
    findAll(find: string) {
    //aca le pegaria al servicio/DB que me traiga las locations --> /locality en Java
    if (find !== undefined && find.trim().length > 0) {
      if (find.length < 4) {
        throw new NotAcceptableException('Error in find parameter', { cause: new Error(), description: 'Must contains at least 4 letters'});
      } else {
        const locationsArray = this.locationsMock.filter((location) =>
            location.province.includes(find) ||
            location.department.includes(find) ||
            location.locality.includes(find)
        );
        return locationsArray;
      }
    } else {
      throw new NotAcceptableException('Error in find parameter', { cause: new Error(), description: 'Cannot be empty' });
    }
  }

  // findOne(@Param('location') location: string) {
  //   return `This action returns a #${location} location`;
  // }

  // update(id: number, updateLocationDto: UpdateLocationDto) {
  //   return `This action updates a #${id} location`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} location`;
  // }
}
