import { Controller, Get, Query } from '@nestjs/common';
import { LocationsService } from './locations.service';

@Controller('locations')
export class LocationsController {
  constructor(private readonly locationsService: LocationsService) {}

  @Get()
  findAll(@Query('find') find: string) {
    // return 'Hello world - Locations controller'
    return this.locationsService.findAll(find);
  }

  // @Post()
  // create(@Body() createLocationDto: CreateLocationDto) {
  //   return this.locationsService.create(createLocationDto);
  // }

  // @Get()
  // findAll() {
  //   return this.locationsService.findAll();
  // }


  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateLocationDto: UpdateLocationDto) {
  //   return this.locationsService.update(+id, updateLocationDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.locationsService.remove(+id);
  // }
}
